package streamer

import (
	"bytes"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

func assertEqual(t *testing.T, got, expected string) {
	if expected != got {
		t.Errorf("expected:\n%v but got:\n%v", expected, got)
	}
}

func requireNoError(t *testing.T, err error) {
	if err != nil {
		t.Fatalf("got an unexpected error: %v", err)
	}
}

func TestPersonNetID(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		if _, err := io.Copy(
			w,
			bytes.NewReader([]byte(`[{"ldapkey":"123","sn":"Smith","givenName":"John","duid":"4321","netid":"johns","display_name":"John Smith (they/them)","nickname":"John"}]`)),
		); err != nil {
			panic(err)
		}
	}))
	c := mustNew(WithBaseURL(srv.URL), WithKey("some-token"))
	got, err := c.NetID("example")
	requireNoError(t, err)
	assertEqual(t, got.DisplayName, "John Smith (they/them)")
}

func TestPersonDUID(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		if _, err := io.Copy(
			w,
			bytes.NewReader([]byte(`{"ldapkey":"123","sn":"Smith","givenName":"John","duid":"4321","netid":"johns","display_name":"John Smith (they/them)","nickname":"John"}`)),
		); err != nil {
			panic(err)
		}
	}))
	c := mustNew(
		WithBaseURL(srv.URL),
		WithKey("foo-key"),
	)
	got, err := c.DUID("example")
	requireNoError(t, err)
	assertEqual(t, got.DisplayName, "John Smith (they/them)")
}

func TestQuery(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, _ *http.Request) {
		if _, err := io.Copy(
			w,
			bytes.NewReader([]byte(`[{"ldapkey":"123"},{"ldapkey":"456"}]`)),
		); err != nil {
			panic(err)
		}
	}))
	c := mustNew(
		WithBaseURL(srv.URL),
		WithKey("foo"),
	)
	got, err := c.Query("example")
	requireNoError(t, err)
	assertEqual(t, got[0].LDAPKey, "123")
	assertEqual(t, got[1].LDAPKey, "456")
}
