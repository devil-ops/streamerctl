# StreamerCTL

Lookup person and netid information on streamer.oit.duke.edu

Requires a streamer token, that can be acquired [here](https://streamer.oit.duke.edu/dev_console/api_keys/new)

## Set Up

Set up your streamer token, either in the local config file, or in your environment:

Environment Example:

```
$ export STREAMERTOKEN=my-token
```

Config File Example:
```
$ cat ~/.streamerctl.yaml
---
streamertoken: my-token
```

## Installation

Install latest version with Go get

```
$ go env -w GO111MODULE=on
$ go get gitlab.oit.duke.edu/devil-ops/streamerctl/streamerctl
```

Install a tagged version from the [package registry](https://gitlab.oit.duke.edu/devil-ops/streamerctl/-/packages) by just downloading the binary

## Usage

Get a specific netid with:

```
$ streamerctl get netid SOME_NETID
```

Do a person search with:

```
$ streamerctl search "some person"
```

*This code is freely available for non-commercial use and is provided as-is with no warranty.*
