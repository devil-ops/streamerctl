/*
Package streamer defines the pieces needed to interact with the streamer service
*/
package streamer

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log/slog"
	"net/http"
	"os"
	"time"

	"moul.io/http2curl"
)

// Client is the thing that does the streamer api calls
type Client struct {
	key        string
	baseURL    string
	printCurl  bool
	httpClient *http.Client
}

func mustNew(opts ...func(*Client)) *Client {
	got, err := New(opts...)
	if err != nil {
		panic(err)
	}
	return got
}

// New returns a new Streamer client using functional options
func New(opts ...func(*Client)) (*Client, error) {
	c := &Client{
		httpClient: &http.Client{
			Timeout: time.Minute,
		},
		baseURL: "https://streamer.oit.duke.edu",
	}
	for _, opt := range opts {
		opt(c)
	}
	if c.key == "" {
		return nil, errors.New("must set streamer token")
	}
	return c, nil
}

// WithPrintCurl sets requests to have a curl command printed
func WithPrintCurl() func(*Client) {
	return func(c *Client) {
		c.printCurl = true
	}
}

// WithKey sets the streamer key for a new client
func WithKey(s string) func(*Client) {
	return func(c *Client) {
		c.key = s
	}
}

// WithBaseURL sets the base URL for streamer
func WithBaseURL(s string) func(*Client) {
	return func(c *Client) {
		c.baseURL = s
	}
}

func injectToken(req *http.Request, t string) {
	eq := req.URL.Query()
	eq.Add("access_token", t)
	req.URL.RawQuery = eq.Encode()
}

func (c *Client) sendRequest(req *http.Request, v any) error {
	injectToken(req, c.key)
	if c.printCurl {
		command, _ := http2curl.GetCurlCommand(req)
		fmt.Fprintf(os.Stderr, "%v\n", command)
	}
	res, err := c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer dclose(res.Body)

	if res.StatusCode < http.StatusOK || res.StatusCode >= http.StatusBadRequest {
		var errRes errorResponse
		if err := json.NewDecoder(res.Body).Decode(&errRes); err == nil {
			return errors.New(errRes.Error)
		}

		if res.StatusCode == http.StatusTooManyRequests {
			return fmt.Errorf("too many requests.  Check rate limit and make sure the userAgent is set right")
		}
		return fmt.Errorf("unknown error, status code: %d", res.StatusCode)
	}

	return json.NewDecoder(res.Body).Decode(&v)
}

// PersonLookup is the signature for a function that looks up an ldap user
type PersonLookup func(string) (*Person, error)

// NetID looks up ldap information on a given netid
func (c *Client) NetID(netid string) (*Person, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%v/ldap/people/netid/%s", c.baseURL, netid), nil)
	if err != nil {
		return nil, err
	}
	var ldapUsers PersonList
	if err := c.sendRequest(req, &ldapUsers); err != nil {
		return nil, err
	}

	switch {
	case len(ldapUsers) == 0:
		return nil, fmt.Errorf("no users with that netid found: %v", netid)
	case len(ldapUsers) > 1:
		return nil, fmt.Errorf("expected a single result, but got: %v", len(ldapUsers))
	default:
		return &ldapUsers[0], nil
	}
}

// DUID looks up an ldap user based on DUID
func (c *Client) DUID(key string) (*Person, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%v/ldap/people/duid/%s", c.baseURL, key), nil)
	if err != nil {
		return nil, err
	}
	var ldapUser Person
	if err := c.sendRequest(req, &ldapUser); err != nil {
		return nil, err
	}
	return &ldapUser, nil
}

// Query queries the person service
func (c *Client) Query(q string) (PersonResultList, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/ldap/people?q=%v", c.baseURL, q), nil)
	if err != nil {
		return nil, err
	}

	var ret PersonResultList
	if err := c.sendRequest(req, &ret); err != nil {
		return nil, err
	}
	return ret, nil
}

// Person describes user attributes we get from the ldap endpoint
type Person struct {
	Address            []string `json:"address,omitempty"`
	Department         string   `json:"department,omitempty"`
	DisplayName        string   `json:"display_name,omitempty"`
	DUID               string   `json:"duid,omitempty"`
	Emails             []string `json:"emails,omitempty"`
	GivenName          string   `json:"givenName,omitempty"`
	LDAPkey            string   `json:"ldapkey,omitempty"`
	NetID              string   `json:"netid,omitempty"`
	Nickname           string   `json:"nickname,omitempty"`
	Phones             []string `json:"phones,omitempty"`
	PrimaryAffiliation string   `json:"primary_affiliation,omitempty"`
	SN                 string   `json:"sn,omitempty"`
	Titles             []string `json:"titles,omitempty"`
	URL                string   `json:"url,omitempty"`
}

// PersonList represents multiple LDAPUser objects
type PersonList []Person

// PersonResult is a result of a person search
type PersonResult struct {
	DisplayName string `json:"display_name,omitempty"`
	DUID        string `json:"duid,omitempty"`
	GivenName   string `json:"givenName,omitempty"`
	LDAPKey     string `json:"ldapkey,omitempty"`
	NetID       string `json:"netid,omitempty"`
	SN          string `json:"sn,omitempty"`
	URL         string `json:"url,omitempty"`
}

// PersonResultList is multiple PersonResult items
type PersonResultList []PersonResult

func dclose(c io.Closer) {
	if err := c.Close(); err != nil {
		slog.Warn("error closing item")
	}
}

type errorResponse struct {
	Error string `json:"error"`
}
