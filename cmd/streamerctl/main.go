/*
Package main is the main executable
*/
package main

import "gitlab.oit.duke.edu/devil-ops/streamerctl/cmd/streamerctl/cmd"

func main() {
	cmd.Execute()
}
