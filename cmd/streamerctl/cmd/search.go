package cmd

import (
	"log/slog"
	"strings"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	streamer "gitlab.oit.duke.edu/devil-ops/streamerctl"
)

// searchCmd represents the search command
func newSearchCmd() *cobra.Command {
	return &cobra.Command{
		Use:     "search",
		Short:   "Search for a user",
		Args:    cobra.MinimumNArgs(1),
		Example: `$ streamerctl search some user`,
		Long: `Search through directory using the given terms. Note that multiple arguments will be combined as a space separated search term. So
streamerctl search "joe smith"

is the same as

streamerctl search joe smith
`,
		RunE: func(_ *cobra.Command, args []string) error {
			sTerm := strings.Join(args, " ")
			peopleResults, err := client.Query(sTerm)
			if err != nil {
				return err
			}
			ldapUsers := make(streamer.PersonList, len(peopleResults))
			slog.Info("found matches, looking up more information on them", "count", len(peopleResults), "search-term", sTerm)
			for idx, personMeta := range peopleResults {
				p, err := client.DUID(personMeta.DUID)
				if err != nil {
					return err
				}
				ldapUsers[idx] = *p
			}
			return gout.Print(ldapUsers)
		},
	}
}
