/*
Package cmd holds all of the command pieces
*/
package cmd

import (
	"log/slog"
	"os"
	"time"

	"github.com/charmbracelet/log"
	goutbra "github.com/drewstinnett/gout-cobra"
	"github.com/spf13/cobra"

	streamer "gitlab.oit.duke.edu/devil-ops/streamerctl"
)

var (
	verbose bool
	version = "dev"
)

var client *streamer.Client

// rootCmd represents the base command when called without any subcommands
func newRootCmd() *cobra.Command {
	cmd := &cobra.Command{
		Use:           "streamerctl",
		Short:         "Interact with streamer.oit.duke.edu service",
		SilenceErrors: true,
		SilenceUsage:  true,
		Version:       version,
		PersistentPreRunE: func(cmd *cobra.Command, _ []string) error {
			if err := goutbra.Cmd(cmd); err != nil {
				return err
			}
			opts := []func(*streamer.Client){streamer.WithKey(os.Getenv("STREAMERTOKEN"))}
			if os.Getenv("DEBUG_CURL") != "" {
				opts = append(opts, streamer.WithPrintCurl())
			}
			var err error
			if client, err = streamer.New(opts...); err != nil {
				return err
			}
			return nil
		},
	}
	cmd.AddCommand(
		newGetCmd(),
		newSearchCmd(),
	)
	if err := goutbra.Bind(cmd); err != nil {
		panic(err)
	}
	return cmd
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := newRootCmd().Execute(); err != nil {
		slog.Error("fatal error", "error", err)
		os.Exit(2)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	lopts := log.Options{
		TimeFormat: time.StampMilli,
		Prefix:     "streamerctl ",
	}
	if verbose {
		lopts.Level = log.DebugLevel
	}
	slog.SetDefault(slog.New(log.NewWithOptions(
		os.Stderr,
		lopts,
	)))
}
