package cmd

import (
	"strconv"

	"github.com/drewstinnett/gout/v2"
	"github.com/spf13/cobra"
	streamer "gitlab.oit.duke.edu/devil-ops/streamerctl"
)

func newGetCmd() *cobra.Command {
	return &cobra.Command{
		Use:   "get NETID|DUID",
		Short: "Get some user info out of streamer",
		Args:  cobra.MinimumNArgs(1),
		Long:  `Do a lookup of something in streamer using either a NetID or Duke Unique ID (DUID)`,
		RunE: func(_ *cobra.Command, args []string) error {
			res := make([]streamer.Person, len(args))
			for idx, item := range args {
				var l streamer.PersonLookup
				if _, err := strconv.Atoi(args[0]); err != nil {
					l = client.NetID
				} else {
					l = client.DUID
				}
				p, err := l(item)
				if err != nil {
					return err
				}
				res[idx] = *p
			}
			return gout.Print(res)
		},
	}
}
